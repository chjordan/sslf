#!/usr/bin/env python

# A little script to generate the plot used in the paper.

import numpy as np
import matplotlib.pyplot as plt
from sslf.sslf import Spectrum, find_background_rms

x, y = np.loadtxt("../notebooks/spectrum.txt", unpack=True)
x = x / 1000
fig, ax = plt.subplots(4, figsize=(8.5, 10), sharex=True, sharey=True)
ax[0].set_xlim((-120, 50))

fig.add_subplot(111, frameon=False)
# plt.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
# plt.tick_params(length=0, labelcolor="none", top='off', bottom='off', left='off', right='off')
plt.tick_params(length=0, labelcolor="none")
plt.grid(False)
plt.ylabel("Brightness temperature [K]")
tmp = plt.gca()
tmp.yaxis.set_label_coords(-0.1, 0.5)

# Naive approach.
rms = find_background_rms(y, 5, 3)
peaks = np.where(y / rms > 3)[0]
ax[0].plot(x[peaks], y[peaks], marker='o', c='r', ms=5, mew=4, ls="None", label="naive; SNR = 3")
ax[0].axhline(3 * rms, xmin=ax[0].get_xlim()[0], xmax=ax[0].get_xlim()[1], c='r')
ax[0].legend()

peaks = np.where(y / rms > 4)[0]
ax[1].plot(x[peaks], y[peaks], marker='o', c='r', ms=5, mew=4, ls="None", label="naive; SNR = 4")
ax[1].axhline(4 * rms, xmin=ax[0].get_xlim()[0], xmax=ax[0].get_xlim()[1], c='r')
ax[1].legend()

peaks = np.where(y / rms > 5)[0]
ax[2].plot(x[peaks], y[peaks], marker='o', c='r', ms=5, mew=4, ls="None", label="naive; SNR = 5")
ax[2].axhline(5 * rms, xmin=ax[0].get_xlim()[0], xmax=ax[0].get_xlim()[1], c='r')
ax[2].legend()

# sslf
s = Spectrum(y, vel=x)
s.find_cwt_peaks(scales=np.arange(2, 60, 2), snr=6.5)
peaks = s.channel_peaks
ax[3].plot(x[peaks], y[peaks], marker='o', c='r', ms=10, mew=4, ls="None", label="sslf; SNR = 6.5")
ax[3].legend()

for i in range(ax.shape[0]):
    ax[i].plot(x, y)

ax[-1].set_xlabel("Velocity [km/s]")

# plt.show()
plt.tight_layout()
fig.subplots_adjust(hspace=0, wspace=0)
plt.savefig("line_finding.pdf")
plt.close()
