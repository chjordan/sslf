\documentclass[RNAAS]{aastex63}

\usepackage{amsmath}
\usepackage{enumitem}
\usepackage{hyperref}
\usepackage{url}
\usepackage{graphicx}

\newcommand{\sslf}{\textsc{sslf}}

\begin{document}
\title{\sslf{}: A simple spectral-line finder}
\correspondingauthor{Christopher H. Jordan}
\email{christopher.jordan@curtin.edu.au}

\author[0000-0002-1220-2940]{Christopher H. Jordan}
\affiliation{International Centre for Radio Astronomy Research (ICRAR), Curtin University, Bentley WA, Australia}
\affiliation{ARC Centre of Excellence for All Sky Astrophysics in 3 Dimensions (ASTRO 3D), Bentley, Australia}
\author[0000-0001-8845-1225]{Bradley W. Meyers}
\affiliation{International Centre for Radio Astronomy Research (ICRAR), Curtin University, Bentley WA, Australia}
\affiliation{Department of Physics \& Astronomy, University of British Columbia, 6224 Agricultural Road, Vancouver, BC V6T 1Z1, Canada}

\keywords{Spectral line identification (2073) --- Wavelet analysis (1918) --- Astronomy software (1855) --- Open source software (1866)}

\section{Motivation}
In astronomical terms, a spectral line is the signature of a physical process in
a spectrum. It can indicate the abundance and velocity of molecules associated
with high-mass star forming regions, for example. More broadly, a spectral line
is simply a relatively-narrow (compared to the size of the spectrum) but
statistically-significant feature in data. The automated identification of
spectral lines is particularly important for large-scale data sets, as it is
difficult to classify lines accurately and consistently, and humans can be
biased as well as inconsistent in their classification.

Given a spectrum (one-dimensional data) and a cut-off significance (a
signal-to-noise ratio as a single positive number, e.g. 5), the simplest method
to identify a spectral line is:

\begin{enumerate}

\item Determine the noise root-mean-square (RMS) of the spectrum ($\sigma$);

\item Divide each sample of the spectrum by the noise RMS ($x / \sigma$);

\item Identify any samples exceeding the cut-off in significance (e.g.
  $x / \sigma > 5$);

\item Characterise and/or attribute the locations of these significant peaks to
  each other (are multiple points in the spectrum related to the same spectral
  line?);

\item (Optional) Repeat steps 3 and 4 for lines exceeding the significance
  cut-off in negative values (e.g. $x / \sigma < -5$).

\end{enumerate}

A number of significant problems arise when attempting to automate
these steps:

\begin{enumerate}

\item If the noise RMS is zero (or even close), then numerical code will not
  behave correctly;

\item If the noise RMS is over-estimated, then to-the-eye significant spectral
  lines may not be identified by the code;

\item If the spectrum has a variable noise level (e.g.\ frequency-dependent
  noise), then an automatically-determined RMS is likely to bias the results of
  line-finding;

\item When considering peak values, the aforementioned spectral-line-finding
  routine is not intelligent in that it does not consider neighbouring values
  when classifying a line; this causes the routine to fail to identify lines
  that are below a threshold cut-off, but are clearly significant from the noise
  to the human eye.

\end{enumerate}

\section{Methodology and Design}
\sslf{} (\url{https://gitlab.com/chjordan/sslf}) attempts to solve these issues
in a simple manner. This is largely achieved by utilising the continuous wavelet
transform (CWT). The CWT can be thought of as a spatially-sensitive Fourier
transform, which ``resonates'' when the input matches the selected wavelet. This
approach allows multiple spectral-line widths to be probed simultaneously, which
in turn robustly attributes multiple spectral peaks to a single spectral line,
and also identifies spectral lines that have statistically-weak peaks but
spatially-correlated information.

The most significant downside to the CWT is that it is parametric; the transform
must be supplied with a wavelet. This in turn requires {\bf a priori} knowledge
of the data to be useful (in particular, the expected shapes of the lines).

\sslf{} was created as there appears to be no other similar tool available. For
this same reason, we cannot gauge its performance in terms of line
identification performance or run-time speed with any other existing
code. However, we are able to gauge its performance against the aforementioned
``naive'' method of line-finding. Figure~\ref{fig:1} shows that \sslf{} is able
to identify relatively weak spectral lines without reporting false positives,
whereas the naive method will either not find all lines or report false
positives.

\begin{figure}[h!]
  \begin{center}
    \includegraphics[scale=0.85,angle=0]{line_finding.pdf}
    \caption{An example of line-finding on a spectrum from the StarFISH
      Galactic-plane survey. The top three panels show the results of ``naive''
      line-finding; all values are simply divided by a RMS and compared with a
      SNR (3, 4, and 5, respectively). The bottom panel shows line-finding with
      \sslf{}, which identifies three spectral lines, whereas the naive approach
      will find these three lines only when the SNR value is dropped, and
      simultaneously finds many false positives. In addition to finding all
      lines without false positives, \sslf{} is better at identifying the centre
      of a spectral line (see the second line). Note that the SNR of \sslf{} is
      quite different to the others as it is used in the wavelet domain, and is
      unfortunately not directly comparable with the naive method.}\label{fig:1}
  \end{center}
\end{figure}

\sslf{} has proven to be effective in identifying thousands of
Gaussian-shaped spectral lines with the Ricker wavelet, as well as
Rayleigh-distributed pulses. It is expected that \sslf{} performs well due
to its use of the CWT, but, as the authors do not have other use cases, we
cannot comment on the performance of \sslf{} at finding, say,
Lorentzian-shaped peaks. However, \sslf{} is also designed to be flexible,
and can find lines with any wavelet compatible with \textsc{scipy.signal.cwt},
and allows a user to fine-tune the determination of a spectrum's noise RMS.\@

In addition to the line-finding capability, \sslf{} can subtract the
low-frequency ``shape'' of a spectrum after it has identified any lines, which
can help to clean the input data for further processing in a pipeline. This
routine could be thought of as a low-order polynomial subtraction from the
spectrum.

\sslf{} was created to automatically identify astronomical spectral lines in the
StarFISH Galactic-plane survey (publication \emph{in prep.}), but is useful for
other spectral-line work, such as MALT-45 \citep{jordan15} and HOPS
\citep{walsh14}.

Further details of usage and performance of \sslf{} is given on the
\href{https://gitlab.com/chjordan/sslf}{project's source code page}, especially
in the ``notebooks'' directory. In particular, the ``completeness'' notebook
shows that \sslf{} is good at identifying weak lines while keeping the reported
number of false positives to a minimum. At the time of writing, the latest
version is v0.2.0 \citep{sslf_zenodo}.

\section{Acknowledgements}
This research was supported by the Australian Research Council Centre of
Excellence for All Sky Astrophysics in 3 Dimensions (ASTRO 3D), through project
number CE170100013. The International Centre for Radio Astronomy Research
(ICRAR) is a Joint Venture of Curtin University and The University of Western
Australia, funded by the Western Australian State government. The authors
acknowledge the contribution of an Australian Government Research Training
Program Scholarship in supporting this research.

\sslf{} is written in \textsc{Python} and utilises the \textsc{NumPy}
\citep{numpy} and \textsc{SciPy} \citep{scipy} libraries, while demonstrations
benefit from the \textsc{Matplotlib} \citep{matplotlib} library.

\bibliography{refs}

\end{document}
