#!/usr/bin/env python

import os
import copy
import numpy as np
from scipy.signal import gaussian

import pytest

from sslf.sslf import Spectrum, _blank_spectrum_part


TEST_DIR = '/'.join(os.path.realpath(__file__).split('/')[0:-1])


def test_manual_rms():
    with open(TEST_DIR + "/spectrum_2.txt", 'r') as f:
        x, y = np.loadtxt(f, unpack=True)
    s = Spectrum(y, vel=x)
    s.find_cwt_peaks(scales=np.arange(10, 30, 2), rms=3e-3, snr=5)
    assert len(s.vel_peaks) == 1
    assert abs(s.peak_snrs[0] - 6.23490) < 1e-3


def test_blank_spectrum_helper():
    np.random.seed(1000)
    s = np.random.normal(size=1000)
    s2 = copy.copy(s)
    # _blank_spectrum_part expects edges to be a tuple, but it should work
    # with an np.ndarray as well.
    edges = np.array([30, 177])
    before = s2[edges[0]:edges[1]]
    _blank_spectrum_part(s, edges, value=0)
    after = s[edges[0]:edges[1]]
    assert np.all(before != after)
    assert np.all(after == 0)

    # Check that edges as a tuple works too.
    edges = (534, 600)
    before = s2[edges[0]:edges[1]]
    _blank_spectrum_part(s, edges, value=0)
    after = s[edges[0]:edges[1]]
    assert np.all(before != after)
    assert np.all(after == 0)


def test_refine_line_widths():
    with open(TEST_DIR + "/spectrum_2.txt", 'r') as f:
        x, y = np.loadtxt(f, unpack=True)
    s = Spectrum(y, vel=x)
    s.find_cwt_peaks(scales=np.arange(10, 30, 2), rms=3e-3, snr=5)
    # These assertions should be the same from `test_manual_rms`.
    assert len(s.vel_peaks) == 1
    assert abs(s.peak_snrs[0] - 6.23490) < 1e-3

    s.refine_line_widths(significance_cutoff=1.5)
    assert s.channel_edges[0] == (274, 315)

    # Use a manually-specified RMS.
    s.refine_line_widths(significance_cutoff=1.5, rms=s.rms * 0.9)
    assert s.channel_edges[0] == (274, 319)


def test_refine_line_widths_absurd():
    """
    Check that a warning is thrown when an absurd RMS value is given.
    """
    np.random.seed(1000)
    absurd = np.abs(np.random.normal(size=1000)) + 10 * gaussian(1000, std=5)
    s = Spectrum(absurd)
    s.find_cwt_peaks(scales=[5], snr=5)

    with pytest.warns(UserWarning) as record:
        s.refine_line_widths(significance_cutoff=1e-6)
        if not record:
            pytest.fail("Expected a warning!")


def test_refine_line_widths_edge_bounds():
    """
    Check that a spectral line's edges do not go out of bounds.
    """
    np.random.seed(1000)
    spec = np.random.normal(size=1000) + \
        np.roll(10 * gaussian(1000, std=8), 490) + \
        np.roll(10 * gaussian(1000, std=8), -490)
    s = Spectrum(spec)
    s.find_cwt_peaks(scales=np.arange(5, 20), snr=5)
    s.refine_line_widths(significance_cutoff=1)


if __name__ == "__main__":
    # Introspect and run all the functions starting with "test".
    for f in dir():
        if f.startswith("test"):
            print(f)
            exec(f+"()")
